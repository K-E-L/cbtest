function hasGetUserMedia() {
    return !!(navigator.mediaDevices &&
              navigator.mediaDevices.getUserMedia);
}

if (hasGetUserMedia()) {
    // Good to go!
} else {
    alert('Use another tool to test the webcam');
}

const constraints = {
    video: true
};

const video = document.querySelector('video');

navigator.mediaDevices.getUserMedia(constraints).
    then((stream) => {video.srcObject = stream});
